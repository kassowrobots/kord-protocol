
#include "kr2/kord/protocol/KORDFrames.h"

#include <iostream>
#include <vector>

#include <gtest/gtest.h>

using namespace kr2::kord::protocol;

TEST(KordFrames, checkSize){

    KORDFrame kord_frame;
    KORDContentFrame content_frame;
    KORDStatusFrame status_frame;

    EXPECT_EQ(MAX_ETH_DATA_LEN_B, sizeof(kord_frame));
    EXPECT_EQ(1430, sizeof(content_frame));
    EXPECT_EQ(1430, sizeof(status_frame));

    EXPECT_EQ(1430, content_frame.getFrameLength());
    EXPECT_EQ(1300, content_frame.getDataLength());

    EXPECT_EQ(1430, status_frame.getFrameLength());
    EXPECT_EQ(1424, status_frame.getDataLength());    
}

