/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_CONTENT_FRAME_PARSER_H
#define KR2_KORD_CONTENT_FRAME_PARSER_H

#include "ContentItem.h"
#include "kr2/kord/protocol/KORDItemIDs.h"

#include <cstddef>
#include <cstdint>

namespace kr2::kord::protocol {

class ContentFrameParser {
public:
    ContentFrameParser();

    ContentFrameParser(const ContentFrameParser &);

    ~ContentFrameParser();

    bool setFromPayload(const uint8_t *payload, size_t dlen);

    //! Return the number of items contained in the frame
    [[nodiscard]] int getItemsCount() const;

    [[nodiscard]] EKORDItemID getItemID(int idx) const;
    [[nodiscard]] ContentItem getItemContent(int idx) const;
    bool getContentItem(int idx, ContentItem *item) const;

    ContentFrameParser &operator=(const ContentFrameParser &);

    bool clear();

private:
    class ContentFrameParserImpl;
    ContentFrameParserImpl *impl_;
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_CONTENT_FRAME_PARSER_H
