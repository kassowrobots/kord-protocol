/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Ondrej Bruna  obr@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_SYSTEM_REQUESTS_H
#define KR2_KORD_SYSTEM_REQUESTS_H

#pragma once

#include <cstdint>

namespace kr2::kord::protocol {

enum EControlCommandItems : uint16_t { // clang-format off
    // Operating system requests
    eInvalid = 0x0000,
    eShutdown = 0x0001,
    eRestart = 0x0002,

    // Responsive controller requests
    eContinueDiscovery = 0x0101,
    eEnableBackDrive = 0x0102,
    eIOSet = 0x0103,

    // Common requests
    eTransferLogFiles = 0x0201,
    eTransferDashboardJson = 0x0202,
    eTransferCalibrationData = 0x0203,
    eServerEnableCommunication = 0x0204,
    eServerDisableCommunication = 0x0205,

    // Files transfer requests
    eTransferFiles = 0x0300,

    // RC API Command requests
    eRCAPICommand = 0x0400,

    // Server Request
    eServer = 0x0500,
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_SYSTEM_REQUESTS_H