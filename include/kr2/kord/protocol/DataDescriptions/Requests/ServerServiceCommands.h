////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2024 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef KR2_KORD_SERVER_SERVICE_COMMANDS_H
#define KR2_KORD_SERVER_SERVICE_COMMANDS_H

#include <cstdint>

namespace kr2::kord::protocol {

enum class EServerServiceCommands : uint16_t {
    // RServer service commands, passed along with service name and parameters
    eStart = 0x0001,
    eStop = 0x0002,
    eGetStatus = 0x0003,
    eGetError = 0x0004,
    eDescribe = 0x0005,
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_SERVER_SERVICE_COMMANDS_H
