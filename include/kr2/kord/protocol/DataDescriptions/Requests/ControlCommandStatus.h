/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Ondrej Bruna  obr@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_CONTROL_COMMAND_STATUS_H
#define KR2_KORD_CONTROL_COMMAND_STATUS_H

#pragma once

#include <cstdint>

namespace kr2::kord::protocol {

enum EControlCommandStatus : uint16_t { eNone = 0, eIdle, eInProgress, eSuccess, eFailure };

} // namespace kr2::kord::protocol

#endif // KR2_KORD_CONTROL_COMMAND_STATUS_H