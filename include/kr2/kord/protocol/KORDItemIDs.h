/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_ITEMIDS_H
#define KR2_KORD_ITEMIDS_H

namespace kr2::kord::protocol {

enum class EKORDItemID : uint32_t { // clang-format off
    eNone                   = 0,
    eCommandMoveJ           = 0x0001,
    eCommandMoveL           = 0x0002,
    eCommandManifold        = 0x0003,

    eCommandSetIODigitalOut = 0x0004,
    eCommandSetIOAnalogOut  = 0x0005,

    eCommandJointFirmware   = 0x0006,

    eCommandMoveDirect      = 0x0007,

    eCommandMoveVelocity    = 0x0008,

    eCommandSetFrame        = 0x0009,
    eCommandSetLoad         = 0x000A,
    eCommandCleanAlarm      = 0x000B,
    eCommandRCAPI           = 0x000C,

    eCommandMoveDirectTorque = 0x000D,

    eRequestStatusArm       = 0x0100,
    eRequestIOStatus        = 0x0101,
    eRequestJointTemp       = 0x0102,
    eRequestJointStatus     = 0x0103,
    eRequestSystem          = 0x0104,
    eRequestTransfer        = 0x0105,
    eRequestServer          = 0x0106,

    eRequestServerStatus    = 0x0107,

    eResponseIOStatus       = 0x0201,

    eStatusEchoResponse     = 0x0300,

    eCustomData             = 0xf000
};

} // namespace kr2::kord::protocol

#endif  // KR2_KORD_ITEMIDS_H
