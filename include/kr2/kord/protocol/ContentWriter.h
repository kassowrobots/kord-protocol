/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_CONTENT_WRITER_H
#define KR2_KORD_CONTENT_WRITER_H

#include <array>
#include <cstdint>
#include <cstring>
#include <vector>

namespace kr2::kord::protocol {

class ContentWriter {
public:
    ContentWriter() = default;

    ContentWriter(uint8_t *head, size_t reserve)
        : content_{head}, capacity_{reserve}, head_{head}, tail_{head_ + reserve}
    {
    }

    void reset(uint8_t *head, size_t reserve)
    {
        content_ = head;
        capacity_ = reserve;
        head_ = head;
        tail_ = head_ + reserve;
    }

    virtual void clear() { content_ = head_; }

    bool addData(const std::array<double, 7> &v)
    {
        if (v.empty())
            return false;
        size_t written = 0;
        size_t len = sizeof(v);

        written = dataWrite(reinterpret_cast<const uint8_t *>(v.data()), len);
        return written == len;
    }

    //! Add either an integral type of arbitrary length or floating
    template <typename T> bool addData(const T &data)
    {
        int written = 0;
        size_t len = sizeof(data);

        written = dataWrite((uint8_t *)&data, len);
        return written == len;
    }

    template <typename T> bool addData(const T &data, unsigned int offset)
    {
        int written = 0;
        if (offset == UINT32_MAX) {
            return false;
        }
        size_t len = sizeof(data);

        written = dataWrite((uint8_t *)&data, offset, len);
        return written == len;
    }

    /**
     * @brief Adds a byte array into the content.
     *
     * @param a_in_data a byte array to be added
     * @param a_offset offset where to place the array
     * @return true if the array was added successfully
     * @return false if the array is too long to be added, or it is empty
     */
    bool addData(const std::vector<uint8_t> &a_in_data, unsigned int a_offset)
    {
        if (head_ + a_offset >= tail_) {
            return false; // writing beyond acceptable region
        }

        if (a_in_data.empty()) {
            return false; // empty array
        }

        if (a_in_data.size() > capacity_) {
            // Not enough space to write
            return false;
        }

        if (a_in_data.size() > (tail_ - (head_ + a_offset))) {
            return false; // there is not enough space in the remaining memory
        }

        // add the array size
        uint16_t in_data_size = a_in_data.size();
        if (dataWrite(reinterpret_cast<uint8_t *>(&in_data_size), a_offset, 2) != 2) {
            return false; // failed to write length
        }

        // write the array itself
        unsigned int array_offset = a_offset + 2;
        unsigned int byte_offset = 0;
        // TODO memcopy directly
        for (auto item : a_in_data) {
            dataWrite(&item, array_offset + byte_offset, 1);
            byte_offset++;
        }
        return true;
    }

private:
    // TODO: add exceptions
    int dataWrite(const uint8_t *data, size_t length)
    {
        if (content_ == nullptr)
            return -2;
        if (length > capacity_)
            return -1;

        memcpy(content_, data, length);
        content_ += length;
        capacity_ -= length;
        return length;
    }

    int dataWrite(const uint8_t *data, unsigned int offset, size_t length)
    {
        if (content_ == nullptr)
            return -2;
        if (length > capacity_)
            return -1;
        if (head_ == nullptr)
            return -3;
        if ((head_ + offset + length) > tail_)
            return -4;

        memcpy(head_ + offset, data, length);
        // check if the offset about to be written is beyond the current
        // keep the highest offset - it has effect on reported size
        // when writing by offset it is desirable to keep the highest written
        uint8_t *pc = content_;
        content_ = head_ + offset + length;
        if (content_ < pc) {
            // restore original pointer
            content_ = pc;
        }
        capacity_ -= length;
        return length;
    }

protected:
    uint8_t *content_{};
    size_t capacity_{0};
    uint8_t *head_{};
    uint8_t *tail_{};
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_CONTENT_WRITER_H
