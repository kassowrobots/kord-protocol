////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2024 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef KR2_KORD_SERVER_SERVICE_STATUS_H
#define KR2_KORD_SERVER_SERVICE_STATUS_H

namespace kr2::kord::protocol {

//! EServiceStatus relates to tracking a state of a service started via ``kr2::kord::RequestServer``.
enum class EServiceStatus : uint8_t {
    eIdle = 1, //!< Service is not running.
    eProgress = 2,  //!< Service is running.
    eSuccess = 3, //!< Service has successfully finished.
    eFailed = 4 //!< An error occurred while running the service.
};

} // kr2::kord::protocol

#endif // KR2_KORD_SERVER_SERVICE_STATUS_H
