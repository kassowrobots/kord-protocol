////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2024 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Author: Daniil Pastukhov  dpa@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////

#ifndef KR2_KORD_SERVER_ITEMIDS_H
#define KR2_KORD_SERVER_ITEMIDS_H

#include <cstdint>

namespace kr2::kord::protocol {

enum class EKORDServerServiceID : uint16_t { // clang-format off
    eLogCollection            = 0x0001,
    eRobotUpdate              = 0x0002,
    eModelUpdate              = 0x0003,
    eProdInfoUpdate           = 0x0004,
    eClusterInit              = 0x0005,
    eFilesUpload              = 0x0006,
    eFilesDownload            = 0x0007,
    eCbunInstall              = 0x0008,
    eFirmwareUpdate           = 0x0009,
    eRobotZeroing             = 0x000A,
    eSaveWorkcell             = 0x000B,
    eLoadWorkcell             = 0x000C,
    eCalibrationDataDownload  = 0x000D,
    eCalibrationDataUpload    = 0x000E,
    eFetchKincalData          = 0x000F,
    eStartupConfigUpdate      = 0x0010,
}; // clang-format on

}

#endif // KR2_KORD_SERVER_ITEMIDS_H
