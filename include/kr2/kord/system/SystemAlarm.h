/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_SYSTEM_ALARMS_H
#define KR2_KORD_SYSTEM_ALARMS_H

#pragma once

namespace kr2::kord::protocol {


/**
 * @brief Category into which the condition ID falls into. Based on this category
 * the condition ID is interpreted.
 * 
 */
enum ESystemAlarmCategory {
    CAT_SAFETY_EVENT = 0x01,       //!< @brief Category for safety events - events that are triggered 
                                   //! by external devices most of the time, for example emregency 
                                   //1 or protective stop. 
    CAT_SOFT_STOP_EVENT = 0x02,    //1< @brief Category for soft stop events - events that are triggered 
                                   //! by the robot controller itself, for example a soft stop due to 
                                   //! a collision detection.
    CAT_HW_STAT = 0x03,            //!< @brief Category for hardware status events - events that are 
                                   //! triggered by the robot controller itself, mostly related to
                                   //! hardware status during initialization.
    CAT_CBUN_EVENT = 0x04          //!< @brief Category for CBun related alarms. 
};

/**
 * @brief Alarm context for system alarms. All safety events are related
 * to current system alarm output.
 * 
 */
enum ESystemAlarmContext {
    CNTXT_ESTOP = 0x01,     //!< @brief The emergency stop is flagged by the system.
    CNTXT_PSTOP = 0x02,     //!< @brief The protective stop is flagged by the system.
    CNTXT_SSTOP = 0x04,     //!< @brief The soft stop is flagged by the system. This is a 
                            //! non-critical stop related to controller itself.
    CNTXT_SYSERR = 0x08     //!< @brief This flag is set when there are hardware error bits set.
};

} // namespace kr2::kord::protocol

#endif //KR2_KORD_SYSTEM_ALARMS_H
