/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2024 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_COMMAND_STATUS_FLAGS_H
#define KR2_KORD_COMMAND_STATUS_FLAGS_H

namespace kr2::kord::protocol {

/**
 * @brief Command status flag represents in what state command has ended up.
 *
 */
enum ECommandStatusFlags {
    COMMAND_STATUS_ACCEPTED =
        0, //!< @brief The command was successfully sent to controller. Note: it is just an indication that controller
           //!< has accepted the command. It does not however represent whether it was handled/finished successfully.
    COMMAND_STATUS_CBUN_ERROR = 1, //!< @brief The error occurred on KORD's side while processing command.
    COMMAND_STATUS_RC_API_ERROR = 2, //!< @brief The error occurred on responsive controller API's side.
    COMMAND_STATUS_CRC_MISMATCH = 3, //!< @brief The command was corrupted during transferring from KORD API to CBun, and CRC does not match.
    COMMAND_STATUS_COMMANDS_OVERLAP = 4, //!< @brief Too many commands were sent at once.
    COMMAND_STATUS_LOST = 5 //!< @brief The command was sent to RC API, but it did not confirm that controller accepted the command within last 20 ticks.
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_COMMAND_STATUS_FLAGS_H