/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Alexander Kazakov aka@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#define MASK_RELAY1 1 << 0
#define MASK_RELAY2 1 << 1
#define MASK_RELAY3 1 << 2
#define MASK_RELAY4 1 << 3

#define MASK_DO1 1 << 4
#define MASK_DO2 1 << 5
#define MASK_DO3 1 << 6
#define MASK_DO4 1 << 7
#define MASK_DO5 1 << 8
#define MASK_DO6 1 << 9
#define MASK_DO7 1 << 10
#define MASK_DO8 1 << 11

#define MASK_TB1 1 << 12
#define MASK_TB2 1 << 13
#define MASK_TB3 1 << 14
#define MASK_TB4 1 << 15

#define MASK_SDO1 1 << 16
#define MASK_SDO2 1 << 17
#define MASK_SDO3 1 << 18
#define MASK_SDO4 1 << 19

enum ESafePortConfiguration {
    eSafePortDisabled = 1,
    eSafePortEnabled = 2,
    eSafePortPStopMapped = 3,
    eSafePortEStopMapped = 4,
    eSafePortBothMapped = 5
};
